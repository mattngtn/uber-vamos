import { createRenderer } from 'fela';
import plugins from 'fela-preset-web';

export default createRenderer({ plugins });
