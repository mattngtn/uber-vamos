// @flow
import * as React from "react";
import type { ComponentType, Node } from "react";
import { RendererProvider } from "react-fela";
import { createRenderer } from "fela";

import Layout from "../components/layout";

type Props = {
  Component: ComponentType<*>,
  pageProps: Props,
};
const renderer = createRenderer();

const MyApp = ({ Component, pageProps }: Props): Node => (
  <RendererProvider renderer={renderer}>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </RendererProvider>
);

export default MyApp;
