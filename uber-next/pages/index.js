// @flow
import * as React from "react";
import { useRouter } from "next/router";
import { type Node, useEffect } from "react";

const Home = (): Node => {
  const router = useRouter();
  useEffect(() => {
    router.push("/orders");
  }, []);

  return <div>HOMEPAGE</div>;
};

export default Home;
