// @flow
import * as React from "react";
import { type Node, useState } from "react";
import { graphql, QueryRenderer } from "react-relay";

import environment from "../../RelayEnvironment";
import HeadTitle from "../../components/headTitle";
import OrderList from "../../components/orders";
import { LIMIT_CHUNK, SORT_PARAM } from "../../constants/limit";

const Orders = (): Node => {
  const [limit, setLimit] = useState(LIMIT_CHUNK);
  return (
    <div>
      <QueryRenderer
        environment={environment}
        fetchPolicy="store-and-network"
        query={graphql`
          query ordersQuery($limit: Int!, $sort: String!) {
            orders(limit: $limit, sort: $sort) {
              id
              title
              description
              amount
            }
          }
        `}
        variables={{
          limit: limit,
          sort: SORT_PARAM
        }}
        render={({ error, props }) => {
          if (error) {
            return <div>Error!</div>;
          }
          if (!props) {
            return <div>Loading...</div>;
          }
          return <OrderList limit={limit} setLimit={setLimit} {...props} />;
        }}
      />
    </div>
  );
};

export default Orders;
