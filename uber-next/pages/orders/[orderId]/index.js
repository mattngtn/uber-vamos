// @flow
import * as React from "react";
import { type Node, useState } from "react";
import { useRouter } from "next/router";
import { useFela } from "react-fela";
import { graphql, QueryRenderer } from "react-relay";

import deleteOrderMutation from "../mutations/deleteOrder";
import environment from "../../../RelayEnvironment";
import HeadTitle from "../../../components/headTitle";
import Order from "../../../components/order";
import Popin from "../../../components/popin";

// style
const ctaGroup = {
  width: "100%",
  display: "flex",
  justifyContent: "space-evenly",
  fontSize: "17px",
  height: "2rem",
};
const cta = {
  border: "none",
  cursor: "pointer",
  color: "white",
  fontSize: "18px",
  paddingLeft: "2rem",
  paddingRight: "2rem",
};
const firstCta = {
  backgroundColor: "red",
};
const secondCta = {
  backgroundColor: "blue",
};

type Props = {
  error: string,
  orderIdExists: boolean,
  props: {
    order: {|
      id: number,
      title: string,
      description: string,
      amount: number,
    |},
  },
};

const OrderPage = (): Node => {
  const [popin, togglePopin] = useState(false);
  const router = useRouter();
  const { css } = useFela();
  const orderIdExists = Boolean(router.query.orderId);

  return orderIdExists ? (
    <div>
      <QueryRenderer
        environment={environment}
        query={graphql`
          query OrderIdQuery($id: ID!) {
            order(id: $id) {
              id
              title
              description
              amount
            }
          }
        `}
        variables={{
          id: router.query.orderId,
        }}
        render={({ error, props }: Props): Node => {
          if (props === null) {
            return null;
          }
          if (error) {
            return <div>Error!</div>;
          }
          return (
            <div>
              <Order {...props.order} />
              <div className={css(ctaGroup)}>
                <button
                  onClick={() => togglePopin(true)}
                  className={css([cta, firstCta])}
                >
                  Supprimer la commande
                </button>
                <button
                  onClick={() => router.push(`/orders/${props.order.id}/edit`)}
                  className={css([cta, secondCta])}
                >
                  Modifier la commande
                </button>
              </div>
              {popin && (
                <Popin
                  togglePopin={togglePopin}
                  action={() =>
                    deleteOrderMutation({
                      variables: {
                        id: props.order.id,
                      },
                      postAction: () => {
                        togglePopin(false);
                        router.push("/orders");
                      },
                    })
                  }
                />
              )}
            </div>
          );
        }}
      />
    </div>
  ) : null;
};

export default OrderPage;
