// @flow
import * as React from "react";
import type { Node } from "react";
import { useRouter } from "next/router";
import { graphql, QueryRenderer } from "react-relay";

import HeadTitle from "../../../components/headTitle";
import Form from "../../../components/form";
import environment from "../../../RelayEnvironment";

const EditOrder = (): Node => {
  const router = useRouter();
  const orderIdExists = Boolean(router.query.orderId);

  return orderIdExists ? (
    <div>
      <QueryRenderer
        environment={environment}
        query={graphql`
          query editFormQuery($id: ID!) {
            order(id: $id) {
              id
              title
              description
              amount
            }
          }
        `}
        variables={{
          id: router.query.orderId,
        }}
        render={({ error, props }): Node => {
          if (props === null) {
            return null;
          }
          if (error) {
            return <div>Error!</div>;
          }
          return <Form action="edit" {...props} />;
        }}
      />
    </div>
  ) : null;
};

export default EditOrder;
