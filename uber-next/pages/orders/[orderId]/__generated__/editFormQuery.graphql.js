/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type editFormQueryVariables = {|
  id: string
|};
export type editFormQueryResponse = {|
  +order: ?{|
    +id: string,
    +title: string,
    +description: ?string,
    +amount: number,
  |}
|};
export type editFormQuery = {|
  variables: editFormQueryVariables,
  response: editFormQueryResponse,
|};
*/


/*
query editFormQuery(
  $id: ID!
) {
  order(id: $id) {
    id
    title
    description
    amount
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "Order",
    "kind": "LinkedField",
    "name": "order",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "title",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "amount",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "editFormQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "editFormQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "93255e1d5cc328b7746b548dc8d5bf49",
    "id": null,
    "metadata": {},
    "name": "editFormQuery",
    "operationKind": "query",
    "text": "query editFormQuery(\n  $id: ID!\n) {\n  order(id: $id) {\n    id\n    title\n    description\n    amount\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'ee5f49c821380f8780c2d86fdee30994';

module.exports = node;
