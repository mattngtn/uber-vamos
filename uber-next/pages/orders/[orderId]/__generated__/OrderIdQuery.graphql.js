/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type OrderIdQueryVariables = {|
  id: string
|};
export type OrderIdQueryResponse = {|
  +order: ?{|
    +id: string,
    +title: string,
    +description: ?string,
    +amount: number,
  |}
|};
export type OrderIdQuery = {|
  variables: OrderIdQueryVariables,
  response: OrderIdQueryResponse,
|};
*/


/*
query OrderIdQuery(
  $id: ID!
) {
  order(id: $id) {
    id
    title
    description
    amount
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "id",
        "variableName": "id"
      }
    ],
    "concreteType": "Order",
    "kind": "LinkedField",
    "name": "order",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "title",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "amount",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "OrderIdQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "OrderIdQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "48502bc54be12d4bb0fff7ab6e4b4485",
    "id": null,
    "metadata": {},
    "name": "OrderIdQuery",
    "operationKind": "query",
    "text": "query OrderIdQuery(\n  $id: ID!\n) {\n  order(id: $id) {\n    id\n    title\n    description\n    amount\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'cb10bf83f53f4906ad57314132348687';

module.exports = node;
