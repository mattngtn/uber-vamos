// @flow
import { commitMutation, graphql } from "react-relay";

import environment from "../../../RelayEnvironment";

type MutationT = {|
  variables: {|
    id: number,
  |},
  postAction: () => void,
|};

const mutation = graphql`
  mutation deleteOrderMutation($id: ID!) {
    deleteOrder(input: { where: { id: $id } }) {
      order {
        id
      }
    }
  }
`;

export default ({ variables, postAction }: MutationT): any =>
  commitMutation(environment, {
    mutation,
    variables,
    onCompleted: (response, errors) => {
      postAction();
    },
    onError: (err) => console.error(err),
  });
