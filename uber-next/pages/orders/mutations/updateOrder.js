// @flow
import { commitMutation, graphql } from "react-relay";

import environment from "../../../RelayEnvironment";
import type { updateOrderMutationVariables } from "../mutations/__generated__/updateOrderMutation.graphql";

type MutationT = {|
  variables: updateOrderMutationVariables,
  postAction: () => void,
|};

const mutation = graphql`
  mutation updateOrderMutation(
    $id: ID!
    $title: String!
    $description: String
    $amount: Int!
  ) {
    updateOrder(
      input: {
        where: { id: $id }
        data: { title: $title, description: $description, amount: $amount }
      }
    ) {
      order {
        id
        title
        amount
        description
      }
    }
  }
`;

export default ({ variables, postAction }: MutationT): any =>
  commitMutation(environment, {
    mutation,
    variables,
    onCompleted: (response, errors) => {
      postAction();
    },
    onError: (err) => console.error(err),
  });
