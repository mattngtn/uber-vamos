// @flow
import { commitMutation, graphql } from "react-relay";

import environment from "../../../RelayEnvironment";
import type { createOrderMutationVariables } from "../mutations/__generated__/createOrderMutation.graphql";

type MutationT = {|
  variables: createOrderMutationVariables,
  postAction: () => void,
|};

const mutation = graphql`
  mutation createOrderMutation(
    $title: String!
    $description: String
    $amount: Int!
  ) {
    createOrder(
      input: {
        data: { title: $title, description: $description, amount: $amount }
      }
    ) {
      order {
        id
        title
        amount
        description
      }
    }
  }
`;

export default ({ variables, postAction }: MutationT): any =>
  commitMutation(environment, {
    mutation,
    variables,
    onCompleted: (response, errors) => {
      postAction();
    },
    onError: (err) => console.error(err),
  });
