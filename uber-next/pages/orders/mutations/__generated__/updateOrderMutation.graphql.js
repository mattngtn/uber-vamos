/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type updateOrderMutationVariables = {|
  id: string,
  title: string,
  description?: ?string,
  amount: number,
|};
export type updateOrderMutationResponse = {|
  +updateOrder: ?{|
    +order: ?{|
      +id: string,
      +title: string,
      +amount: number,
      +description: ?string,
    |}
  |}
|};
export type updateOrderMutation = {|
  variables: updateOrderMutationVariables,
  response: updateOrderMutationResponse,
|};
*/


/*
mutation updateOrderMutation(
  $id: ID!
  $title: String!
  $description: String
  $amount: Int!
) {
  updateOrder(input: {where: {id: $id}, data: {title: $title, description: $description, amount: $amount}}) {
    order {
      id
      title
      amount
      description
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "amount"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "description"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "id"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "title"
},
v4 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "amount",
                "variableName": "amount"
              },
              {
                "kind": "Variable",
                "name": "description",
                "variableName": "description"
              },
              {
                "kind": "Variable",
                "name": "title",
                "variableName": "title"
              }
            ],
            "kind": "ObjectValue",
            "name": "data"
          },
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "id",
                "variableName": "id"
              }
            ],
            "kind": "ObjectValue",
            "name": "where"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "updateOrderPayload",
    "kind": "LinkedField",
    "name": "updateOrder",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Order",
        "kind": "LinkedField",
        "name": "order",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "title",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "amount",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "updateOrderMutation",
    "selections": (v4/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v2/*: any*/),
      (v3/*: any*/),
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "updateOrderMutation",
    "selections": (v4/*: any*/)
  },
  "params": {
    "cacheID": "0943cccf1d01f16f666f10c39e897d5b",
    "id": null,
    "metadata": {},
    "name": "updateOrderMutation",
    "operationKind": "mutation",
    "text": "mutation updateOrderMutation(\n  $id: ID!\n  $title: String!\n  $description: String\n  $amount: Int!\n) {\n  updateOrder(input: {where: {id: $id}, data: {title: $title, description: $description, amount: $amount}}) {\n    order {\n      id\n      title\n      amount\n      description\n    }\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'f1f9d3a62135301848bcf978227dadfc';

module.exports = node;
