/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type createOrderMutationVariables = {|
  title: string,
  description?: ?string,
  amount: number,
|};
export type createOrderMutationResponse = {|
  +createOrder: ?{|
    +order: ?{|
      +id: string,
      +title: string,
      +amount: number,
      +description: ?string,
    |}
  |}
|};
export type createOrderMutation = {|
  variables: createOrderMutationVariables,
  response: createOrderMutationResponse,
|};
*/


/*
mutation createOrderMutation(
  $title: String!
  $description: String
  $amount: Int!
) {
  createOrder(input: {data: {title: $title, description: $description, amount: $amount}}) {
    order {
      id
      title
      amount
      description
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "amount"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "description"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "title"
},
v3 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "amount",
                "variableName": "amount"
              },
              {
                "kind": "Variable",
                "name": "description",
                "variableName": "description"
              },
              {
                "kind": "Variable",
                "name": "title",
                "variableName": "title"
              }
            ],
            "kind": "ObjectValue",
            "name": "data"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "createOrderPayload",
    "kind": "LinkedField",
    "name": "createOrder",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Order",
        "kind": "LinkedField",
        "name": "order",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "title",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "amount",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "createOrderMutation",
    "selections": (v3/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v2/*: any*/),
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "createOrderMutation",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "4136648eb7aef76839da050175effb58",
    "id": null,
    "metadata": {},
    "name": "createOrderMutation",
    "operationKind": "mutation",
    "text": "mutation createOrderMutation(\n  $title: String!\n  $description: String\n  $amount: Int!\n) {\n  createOrder(input: {data: {title: $title, description: $description, amount: $amount}}) {\n    order {\n      id\n      title\n      amount\n      description\n    }\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '33750b56a7ea2589249a2879b9502c6e';

module.exports = node;
