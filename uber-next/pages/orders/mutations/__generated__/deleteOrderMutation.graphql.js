/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type deleteOrderMutationVariables = {|
  id: string
|};
export type deleteOrderMutationResponse = {|
  +deleteOrder: ?{|
    +order: ?{|
      +id: string
    |}
  |}
|};
export type deleteOrderMutation = {|
  variables: deleteOrderMutationVariables,
  response: deleteOrderMutationResponse,
|};
*/


/*
mutation deleteOrderMutation(
  $id: ID!
) {
  deleteOrder(input: {where: {id: $id}}) {
    order {
      id
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "fields": [
          {
            "fields": [
              {
                "kind": "Variable",
                "name": "id",
                "variableName": "id"
              }
            ],
            "kind": "ObjectValue",
            "name": "where"
          }
        ],
        "kind": "ObjectValue",
        "name": "input"
      }
    ],
    "concreteType": "deleteOrderPayload",
    "kind": "LinkedField",
    "name": "deleteOrder",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Order",
        "kind": "LinkedField",
        "name": "order",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "deleteOrderMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "deleteOrderMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "e140b8bf21bf379b930364dd98bd9c09",
    "id": null,
    "metadata": {},
    "name": "deleteOrderMutation",
    "operationKind": "mutation",
    "text": "mutation deleteOrderMutation(\n  $id: ID!\n) {\n  deleteOrder(input: {where: {id: $id}}) {\n    order {\n      id\n    }\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'ed262a9ffdb5ecdd55ab43c5e96ab240';

module.exports = node;
