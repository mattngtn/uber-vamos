// @flow
import * as React from "react";
import type { Node } from "react";
import { useRouter } from "next/router";

import HeadTitle from "../../components/headTitle";
import Form from "../../components/form";

const CreateOrder = (): Node => {
  const router = useRouter();

  const initialProps = {
    id: "",
    title: "",
    description: "",
    amount: 0,
  };

  return (
    <div>
      <Form action="create" order={initialProps} />
    </div>
  );
};

export default CreateOrder;
