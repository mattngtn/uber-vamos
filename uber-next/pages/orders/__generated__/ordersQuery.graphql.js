/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type ordersQueryVariables = {|
  limit: number,
  sort: string,
|};
export type ordersQueryResponse = {|
  +orders: ?$ReadOnlyArray<?{|
    +id: string,
    +title: string,
    +description: ?string,
    +amount: number,
  |}>
|};
export type ordersQuery = {|
  variables: ordersQueryVariables,
  response: ordersQueryResponse,
|};
*/


/*
query ordersQuery(
  $limit: Int!
  $sort: String!
) {
  orders(limit: $limit, sort: $sort) {
    id
    title
    description
    amount
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "limit"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "sort"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "limit",
        "variableName": "limit"
      },
      {
        "kind": "Variable",
        "name": "sort",
        "variableName": "sort"
      }
    ],
    "concreteType": "Order",
    "kind": "LinkedField",
    "name": "orders",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "title",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "amount",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ordersQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ordersQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "c6c7cb18406f7d674c5285aa355135cf",
    "id": null,
    "metadata": {},
    "name": "ordersQuery",
    "operationKind": "query",
    "text": "query ordersQuery(\n  $limit: Int!\n  $sort: String!\n) {\n  orders(limit: $limit, sort: $sort) {\n    id\n    title\n    description\n    amount\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = 'f44df03cff147009d3363d6630e88de8';

module.exports = node;
