// @flow
import * as React from "react";
import Link from "next/link";
import type { Node } from "react";
import { useFela } from "react-fela";
import { useRouter } from "next/router";

import Order from "../order";
import { LIMIT_CHUNK } from "../../constants/limit";

// style
const listHeader = {
  display: "flex",
  justifyContent: "space-between",
};
const list = {
  paddingLeft: "0",
  listStyle: "none",
};
const listLink = {
  cursor: "pointer",
};

const button = {
  height: "100%",
  padding: "0.7rem",
  margin: "auto 0",
  border: "none",
  backgroundColor: "#06C167",
  color: "white",
  cursor: "pointer",
};

const centeredElement = {
  display: "block",
  margin: "auto"
}

type Props = {
  orders: Array<{|
    title: string,
    description: string,
    amount: number,
    id: number,
  |}>,
  setLimit: (any) => void,
  limit: number,
};

const OrderList = ({ orders, limit, setLimit }: Props): Node => {
  const { css } = useFela();
  const router = useRouter();

  return (
    <div>
      <div className={css(listHeader)}>
        <h2>Commandes en cours</h2>
        <button
          className={css(button)}
          onClick={() => router.push("/orders/create")}
        >
          Ajouter une commande
        </button>
      </div>
      <div>
        <ul className={css(list)}>
          {orders.map((order) => (
            <Link key={order.id} href={`/orders/${order.id}`}>
              <li className={css(listLink)}>
                <Order {...order} />
              </li>
            </Link>
          ))}
        </ul>
      </div>
      <button
        className={css([button, centeredElement])}
        onClick={() => setLimit(limit += LIMIT_CHUNK)}
      >
        Afficher plus
      </button>
    </div>
  );
};

export default OrderList;
