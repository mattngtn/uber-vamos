import validateModel from "../validators";

describe("validateModel", () => {
  it("should return nothing if fields are valid", () => {
    const expected = [];
    const fields = { title: "foo", description: "bar", amount: 1 };
    expect(validateModel(fields)).toEqual(expected);
  });

  it(" should return errors if fields are empty", () => {
    const expected = [
      { error: "amountError", field: "amount" },
      { error: "missing", field: "title" },
    ];
    const fields = { title: null, description: "bar", amount: null };
    expect(validateModel(fields)).toEqual(expected);
  });

  it("should return error if title is empty", () => {
    const expected = [{ error: "missing", field: "title" }];
    const fields = { title: null, description: "bar", amount: 1 };
    expect(validateModel(fields)).toEqual(expected);
  });

  it("should return error if amount is less or equal than 0", () => {
    const expected = [{ error: "amountError", field: "amount" }];
    const fields = { title: "foo", description: "bar", amount: 0 };
    expect(validateModel(fields)).toEqual(expected);
  });
});
