import { isFieldOnError } from "../helpers";

describe("isFieldOnError", () => {
  it("should return true if field is included in errors", () => {
    expect(
      isFieldOnError({
        field: "title",
        errors: [{ field: "title", error: "missing" }],
      })
    ).toBe(true);
  });
  it("should return false if field is not included in errors", () => {
    expect(isFieldOnError({ field: "title", errors: [] })).toBe(false);
  });
});
