// @flow

type IsFieldOnErrorT = {|
  field: string,
  errors: $ReadOnlyArray<{ field: string, error: string }>,
|};

export const isFieldOnError = ({ field, errors }: IsFieldOnErrorT): boolean =>
  errors.some((err) => err.field === field);
