// @flow
import * as React from "react";
import { type Node, useState, useReducer } from "react";
import { useRouter } from "next/router";
import { useFela } from "react-fela";

import type { updateOrderMutationVariables } from "../../pages/orders/mutations/__generated__/updateOrderMutation.graphql";
import formValidator from "./validators";
import createOrderMutation from "../../pages/orders/mutations/createOrder";
import updateOrderMutation from "../../pages/orders/mutations/updateOrder";
import { isFieldOnError } from "./helpers";

// style
const formContainer = {
  padding: "3rem",
  border: "1px solid",
};
const formFlex = {
  display: "flex",
  flexDirection: "column",
};
const formElement = {
  fontSize: "20px",
};
const formInputGroup = {
  margin: "1rem 0",
};
const formInput = {
  lineHeight: "1.5rem",
};
const formInputNumber = {
  width: "10%",
};

const errorMessage = {
  color: "red",
};
const button = {
  width: "20%",
  margin: "auto",
  padding: "0.7rem",
  fontSize: "18px",
  border: "none",
  cursor: "pointer",
  backgroundColor: "blue",
  color: "white",
};

type Props = {|
  action: "create" | "edit",
  order: updateOrderMutationVariables,
|};

type StateT = {|
  title: string,
  description: ?string,
  amount: number,
  isAmountOnError: boolean,
  isTitleOnError: boolean,
|};

type ActionT = {|
  type: string,
  title?: string,
  description?: string,
  amount?: number,
  isAmountOnError?: boolean,
  isTitleOnError?: boolean,
|};

const reducer = (state: StateT, action: ActionT) => {
  switch (action.type) {
    case "SET_TITLE":
      return {
        ...state,
        title: action.title,
      };
    case "SET_DESCRIPTION":
      return {
        ...state,
        description: action.description,
      };
    case "SET_AMOUNT":
      return {
        ...state,
        amount: action.amount,
      };
    case "SET_AMOUNT_ON_ERROR":
      return {
        ...state,
        isAmountOnError: action.isAmountOnError,
      };
    case "SET_TITLE_ON_ERROR":
      return {
        ...state,
        isTitleOnError: action.isTitleOnError,
      };
    default:
      return {
        ...state,
      };
  }
};

const Form = ({ action, order }: Props): Node => {
  const initialState: StateT = {
    title: order.title,
    description: order.description,
    amount: order.amount,
    isAmountOnError: false,
    isTitleOnError: false,
  };

  // $FlowFixMe
  const [state, dispatch] = useReducer(reducer, initialState);

  const router = useRouter();
  const { css } = useFela();
  const { title, description, amount, isAmountOnError } = state;

  const onChange = (field: string, action: string) => (e) => {
    dispatch({ type: action, [field]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const errors = formValidator({ title, description, amount });
    if (errors?.length > 0) {
      const isAmountOnError = isFieldOnError({ field: "amount", errors });
      if (isAmountOnError) {
        dispatch({ type: "SET_AMOUNT_ON_ERROR", isAmountOnError: true });
      }
      const isTitleOnError = isFieldOnError({ field: "title", errors });
      if (isTitleOnError) {
        dispatch({ type: "SET_TITLE_ON_ERROR", isTitleOnError: true });
      }
      return;
    } else {
      dispatch({ type: "SET_AMOUNT_ON_ERROR", isAmountOnError: false });
      dispatch({ type: "SET_TITLE_ON_ERROR", isTitleOnError: false });
    }

    const quantity = Number(amount);
    const { orderId } = router.query;
    if (action === "edit") {
      updateOrderMutation({
        variables: {
          id: orderId,
          title,
          description,
          amount: quantity,
        },
        postAction: () => {
          router.push(`/orders/${orderId}`);
        },
      });
    } else if (action === "create") {
      createOrderMutation({
        variables: {
          title,
          description,
          amount: quantity,
        },
        postAction: () => {
          router.push("/orders");
        },
      });
    }
  };

  return (
    <div>
      <div className={css([formFlex, formContainer])}>
        <div>
          {action === "create" ? (
            <h2>Ajouter une commande</h2>
          ) : (
            <h2>Modifier une commande</h2>
          )}
        </div>
        <form className={css([formFlex, formElement])} onSubmit={handleSubmit}>
          <div className={css([formFlex, formInputGroup])}>
            <label htmlFor="title">Titre</label>
            <input
              className={css(formInput)}
              onChange={onChange("title", "SET_TITLE")}
              id="title"
              type="text"
              autoComplete="title"
              value={title}
            />
          </div>
          <div className={css([formFlex, formInputGroup])}>
            <label htmlFor="desc">Informations complémentaires</label>
            <textarea
              className={css(formInput)}
              onChange={onChange("description", "SET_DESCRIPTION")}
              id="desc"
              type="textarea"
              autoComplete="desc"
              value={description}
            />
          </div>
          <div className={css([formFlex, formInputGroup])}>
            <label htmlFor="amount">Quantité</label>
            <input
              className={css([formInput, formInputNumber])}
              onChange={onChange("amount", "SET_AMOUNT")}
              id="amount"
              type="number"
              autoComplete="amount"
              value={amount}
              required
            />
            {isAmountOnError && (
              <div className={css(errorMessage)}>
                Ce champs n'est pas valide
              </div>
            )}
          </div>
          {action === "create" ? (
            <button className={css(button)} type="submit">
              Ajouter la commande
            </button>
          ) : (
            <button className={css(button)} type="submit">
              {action === "create"
                ? "Ajouter la commande"
                : "Modifier la commande"}
            </button>
          )}
        </form>
      </div>
    </div>
  );
};

export default Form;
