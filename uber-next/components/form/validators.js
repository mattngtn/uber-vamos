// @flow
import {
  validateModel,
  createValidation,
  validateRequired,
  type ModelValidatorT,
} from "@kilix/functional-validation";

const validator: ModelValidatorT<Object, Array<void>> = validateModel([
  createValidation(
    "amount", // used to be able to set the field value of the error
    (model) => model.amount, // returns the part of the model that will be checked
    (amount) => (amount > 0 ? null : "amountError")
  ),
  validateRequired("title"),
]);

export default validator;
