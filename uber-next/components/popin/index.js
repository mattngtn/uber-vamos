// @flow
import * as React from "react";
import type { Node } from "react";
import { useFela } from "react-fela";

// style
const popinContainer = {
  position: "absolute",
  top: "1px",
  left: "1px",
  width: "100vw",
  height: "100vh",
  opacity: "0.7",
  backgroundColor: "lightgray",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const popin = {
  opacity: "1",
  border: "1px solid black",
  borderRadius: "4px",
  width: "40%",
  backgroundColor: "white",
  zIndex: "100",
};
const popinHeader = {
  display: "block",
  padding: "0.7rem 0",
  borderBottom: "1px solid black",
};

const popinHeaderSpan = {
  margin: "auto",
  width: "18%",
  display: "block",
};
const popinContentSpan = {
  margin: "3rem 4rem",
  display: "block",
};

const popinContentButtons = {
  display: "flex",
  justifyContent: "space-evenly",
  width: "30%",
  margin: "1rem auto",
};

const popinContentButton = {
  padding: "0.5rem 2rem",
  margin: "0.7rem",
  backgroundColor: "white",
  border: "1px solid black",
  cursor: "pointer",
};

type Props = {
  togglePopin: (boolean) => void,
  action: () => void,
};

const Popin = ({ togglePopin, action }: Props): Node => {
  const { css } = useFela();

  return (
    <div className={css(popinContainer)}>
      <div className={css(popin)}>
        <div className={css(popinHeader)}>
          <span className={css(popinHeaderSpan)}>Confirmation</span>
        </div>
        <div>
          <span className={css(popinContentSpan)}>
            Êtes-vous sûr(e) de vouloir supprimer cette commande ?
          </span>
          <div className={css(popinContentButtons)}>
            <button onClick={action} className={css(popinContentButton)}>
              Yes
            </button>
            <button
              onClick={() => togglePopin(false)}
              className={css(popinContentButton)}
            >
              No
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Popin;
