// @flow
import * as React from "react";
import type { Node } from "react";
import { useFela } from "react-fela";

// style
const listItem = {
  border: "1px solid black",
  padding: "1rem",
  margin: "1rem 0",
  height: "6rem"
};
const listItemSpan = {
  backgroundColor: "black",
  color: "white",
  padding: "0.5rem",
};
const listItemDesc = {
  fontSize: "14px",
  maxWidth: "50%",
  margin: "1rem 0",
};
const listItemNumber = {
  marginLeft: "1rem",
  backgroundColor: "#06C167",
  color: "white",
};
const listItemQuantity = {
  display: "block",
  marginLeft: "auto",
  marginRight: "0",
  width: "10%",
};

type Props = {|
  title: string,
  description: string,
  amount: number,
  id: number,
|};

const Order = ({ title, description, amount, id }: Props): Node => {
  const { css } = useFela();

  return (
    <div className={css(listItem)}>
      <span className={css(listItemSpan)}>{title}</span>
      <p className={css(listItemDesc)}>{description}</p>
      <span className={css(listItemQuantity)}>
        Quantité :
        <span className={css([listItemSpan, listItemNumber])}>{amount}</span>
      </span>
    </div>
  );
};

export default Order;
