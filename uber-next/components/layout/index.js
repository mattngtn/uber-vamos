// @flow
import * as React from "react";
import Link from "next/link";
import type { Node } from "react";
import { useFela } from "react-fela";

import HeadTitle from "../headTitle";

// style
const container = {
  width: "90%",
  margin: "auto",
  fontFamily: "Rubik, sans-serif",
};
const preTitle = {
  fontWeight: "400",
};
const title = {
  fontWeight: "500",
  color: "#06C167",
  cursor: "pointer",
};

type Props = {|
  children: Node,
|};

const Layout = ({ children }: Props): Node => {
  const { css } = useFela();

  return (
    <div className={css(container)}>
      <HeadTitle />
      <Link href="/orders">
        <h1 className={css(preTitle)}>
          Uber <span className={css(title)}>Vamos</span>
        </h1>
      </Link>
      <main>{children}</main>
    </div>
  );
};

export default Layout;
