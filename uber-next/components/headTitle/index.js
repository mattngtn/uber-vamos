// @flow
import * as React from "react";
import type { Node } from "react";

import Head from "next/head";

const HeadTitle = (): Node => (
  <Head>
    <title>Vamos Uber</title>
    <link rel="icon" href="/favicon.ico" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500&display=swap"
      rel="stylesheet"
    />
  </Head>
);

export default HeadTitle;
